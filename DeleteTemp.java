package webScrapping;

import java.io.File;
import java.util.Calendar;

public class DeleteTemp extends Thread {
	private Thread deleteFile;
	private String threadName;
	
	DeleteTemp(String name)
	{
		this.threadName=name;
	}

	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (deleteFile == null)
		{
			deleteFile = new Thread (this, threadName);
			deleteFile.start ();
		}
	}

	public void run()
	{
		while (true)
		{
			try
			{
				String tempPath=System.getProperty("java.io.tmpdir");
				System.out.println("deleting files at "+tempPath);

				File file = new File(tempPath);

				String[] myFiles;
				if (file.isDirectory()) {

					myFiles = file.list();
					for (int i = 0; i < myFiles.length; i++) {

						File myFile = new File(file, myFiles[i]);
						long lastM=myFile.lastModified();

						Calendar cal = Calendar.getInstance();
						long today=cal.getTimeInMillis();

						long hourDifference=(today-lastM)/3600000;

						if(hourDifference >2)
						{
							System.out.println("today "+today+"  lastMod  "+lastM+"   difference : "+hourDifference+ " hour,  file"+myFile.getName());
							myFile.delete();
						}
					}
				}
				
				Thread.sleep(3600000);// 1 hour
			}catch(Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}
	}

}
