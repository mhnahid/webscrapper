package webScrapping;

public class PathInfo {
	public static String apiKey="4833e5537ae972a643efad33a707d40f";
	
//	public static String browserName="chrome";  //firefox  phantomjs
//	public static String browserName="firefox";  
//	public static String browserName="phantomjs";  //internetExplorer
	public static String browserName="internetExplorer";  //internetExplorer
	public static boolean batchRun=false;
	
	public static int waitTime=30; //unit second
	
	public static int batchWait=4000; // Unit millisecond
	
	public static int mainThreadSleep=1000; // Unit millisecond
	
	public static boolean isCar1=false;
	public static boolean isCar2=false;
	
	public static boolean isNadlan1=false;
	public static boolean isNadlan2=false;
	
	public static boolean isYad1=false;
	public static boolean isYad2=false;
	
	public static boolean isPet1=false;
	public static boolean isPet2=false;

	
	public static String linkCar = "http://www.yad2.co.il/Cars/Car.php";
	public static String linkPet ="http://www.yad2.co.il/Pets/Pets.php";
	public static String linkYad ="http://www.yad2.co.il/Yad2/Yad2.php";
	public static String linkNadlan ="http://www.yad2.co.il/Nadlan/sales.php";
	
	public static String geolink="http://geo.yad2.co.il/";
	public static String nextButton="//*[@id=\"all_results_table\"]/div[5]/table/tbody/tr/td[3]/div";
	// path alternate
	public static String _freqPrimary="/html";
	public static String _freqSubOrdinate="/body";
	public static String _freqTab = "/table";
	public static String _freqMiddle="/tbody";
	public static String _freqDivx="/div";
	
	public static String _freqTr = "/tr";
	public static String _freqTd= "/td";

	public static String _freqAn="/a";
	public static String _freqbn="/b";

	public static String _freqPrefix1 = "/div[";
	public static String _freqSuffix1 = "]";
	
	public static String _freqTabPrefix1 = "/table[";
	public static String _freqTrPrefix1 = "/tr[";
	public static String _freqTdPrefix1 = "/td[";

	public static int _frequency1 = 1;
	public static int _frequency2 = 2;
	public static int _frequency3 = 3;
	public static int _frequency4 = 4;
	public static int _frequency5 = 5;
	public static int _frequency13 = 13;
	public static int _frequency16 = 16;
	public static int _frequency22 = 22;
	
	//car info
	public static String carContactPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[1]/div[2]/div[2]";
	public static String carDetailsPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div[1]/div[2]/div[2]";
	public static String carContactNamePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[1]/div[2]/div[2]/table[3]/tbody/tr[1]/td[2]/b";
	public static String carPhonePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[1]/div[2]/div[2]/table[2]/tbody/tr/td/table/tbody/tr/td[2]/b";
	
	public static String carMoreDetailsPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div[2]/div[2]";
	public static String carPricePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div[1]/div[2]/div[3]/table/tbody/tr/td[2]/table/tbody/tr/td";
	public static String carDescPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[3]/div[2]/div[2]";

	//pet info
	
	public static String petContactPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[1]/div[2]";
	public static String petDetailsPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div/div[2]/div[2]/table/tbody/tr/td[3]";
	public static String petMoreDetailsPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]";
	public static String petPhonePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[1]/div[2]/div[2]/table[2]/tbody/tr/td/table/tbody/tr/td[2]/b";
	
	public static String petPhoneClickPath="//*[@id=\"toShowPhone\"]/a";

	public static String petPricePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div/div[2]/div[2]/div/table/tbody/tr/td[2]";
	
	//Yad
	
	
	public static String yadContactPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[1]/div[2]";
	public static String yadPhonePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div[1]/div[2]/div[2]/table[2]/tbody/tr/td/table/tbody/tr/td[2]/b";
	
//	public static String yadBodyPath="";
	public static String yadDetails="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div[1]/div[2]/div[2]";
	public static String yadPhoneClickPath="//*[@id=\"toShowPhone\"]/a";
	
	public static String yadPricePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div[1]/div[2]/div[3]/table/tbody/tr/td[2]/table/tbody/tr/td[1]";
	public static String yadDescriptionPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[1]/div[2]/div[2]/div[2]";
//	public static String
	
	
	public static String nadContactPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[1]/td[2]/div[1]/div[2]/div[2]";
	public static String nadPhonePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[1]/td[2]/div[1]/div[2]/div[2]/table[1]/tbody/tr/td/table/tbody/tr/td/b";
	
//	public static String yadBodyPath="";
//	public static String yadClickPath="";
	public static String nadPhoneClickPath="//*[@id=\"toShowPhone\"]/a";
	public static String nadDetailsPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[1]/td[1]/div[1]/div[2]/div[2]";
	public static String nadMoreDetailsPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[1]/td[1]/div[2]/div[2]/div[2]";
	
	public static String nadPricePath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[1]/td[1]/div[1]/div[2]/div[3]/table/tbody/tr/td[2]/table/tbody/tr/td/font";
	public static String nadFeatureDetailsPath="//*[@id=\"mainFrame\"]/div[1]/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr[1]/td[1]/div[2]/div[2]/div[2]/div";
//	public static String
}
