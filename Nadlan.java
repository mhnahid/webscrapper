package webScrapping;

public class Nadlan {
	private Contact nadlanContact;
	private String propertyType;
	private String neighborhood;
	private String address;
	private String rooms;
	private String floor;
	private String balconies;
	private int size;
	private long price;
	
int adNumber;
	
	public int getAdNumber() {
		return adNumber;
	}
	public void setAdNumber(int adNumber) {
		this.adNumber = adNumber;
	}
	
private String pubDate;
	
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	
	// checkbox

	private boolean parking;
	private boolean bar;
	private boolean air;
	private boolean dimension;
	private boolean accessForDisabled;
	private boolean elevator;
	private boolean warehouse;
	private boolean sunTerrace;
	private boolean porch;
	private boolean renovated;
	private boolean pandorDoors;
	private boolean uint;
	
	// more
	private String furniture;
	private String availability;
	private String extras;
	private String garden;
	
	private String details;
	private String moreDetails;
	private String featureDetails;
	
	
	public String getBalconies() {
		if(balconies!=null)
		{
			return Utils.fixToSql(this.balconies);
		}
		else return null;
	}
	public void setBalconies(String balconies) {
		this.balconies = balconies;
	}
	public String getDetails() {
		//return details;
		if(details!=null)
		{
			return Utils.fixToSql(this.details);
		}
		else return null;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getMoreDetails() {
		if(moreDetails!=null)
		{
			return Utils.fixToSql(this.moreDetails);
		}
		else return null;
	}
	
	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}
	public String getFeatureDetails() {
		if(featureDetails!=null)
			return Utils.fixToSql(this.featureDetails);
			else return null;
	}
	public void setFeatureDetails(String featureDetails) {
		this.featureDetails = featureDetails;
	}
	
	public String getPropertyType() {
		if(propertyType!=null)
			return Utils.fixToSql(this.propertyType);
			else return null;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public Contact getNadlanContact() {
		return nadlanContact;
	}
	public void setNadlanContact(Contact nadlanContact) {
		this.nadlanContact = nadlanContact;
	}
	public String getNeighborhood() {
		if(neighborhood!=null)
			return Utils.fixToSql(this.neighborhood);
			else return null;
	}
	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}
	public String getAddress() {
//		return address;
		if(address!=null)
			return Utils.fixToSql(this.address);
			else return null;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getRooms() {
		if(rooms!=null)
			return Utils.fixToSql(this.rooms);
			else return null;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getFloor() {
		if(floor!=null)
			return Utils.fixToSql(this.floor);
			else return null;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public boolean isParking() {
		return parking;
	}
	public void setParking(boolean parking) {
		this.parking = parking;
	}
	public boolean isBar() {
		return bar;
	}
	public void setBar(boolean bar) {
		this.bar = bar;
	}
	public boolean isAir() {
		return air;
	}
	public void setAir(boolean air) {
		this.air = air;
	}
	public boolean isDimension() {
		return dimension;
	}
	public void setDimension(boolean dimension) {
		this.dimension = dimension;
	}
	public boolean isAccessForDisabled() {
		return accessForDisabled;
	}
	public void setAccessForDisabled(boolean accessForDisabled) {
		this.accessForDisabled = accessForDisabled;
	}
	public boolean isElevator() {
		return elevator;
	}
	public void setElevator(boolean elevator) {
		this.elevator = elevator;
	}
	public boolean isWarehouse() {
		return warehouse;
	}
	public void setWarehouse(boolean warehouse) {
		this.warehouse = warehouse;
	}
	public boolean isSunTerrace() {
		return sunTerrace;
	}
	public void setSunTerrace(boolean sunTerrace) {
		this.sunTerrace = sunTerrace;
	}
	public boolean isPorch() {
		return porch;
	}
	public void setPorch(boolean porch) {
		this.porch = porch;
	}
	public boolean isRenovated() {
		return renovated;
	}
	public void setRenovated(boolean renovated) {
		this.renovated = renovated;
	}
	public boolean isPandorDoors() {
		return pandorDoors;
	}
	public boolean isUint() {
		return uint;
	}
	public void setUint(boolean uint) {
		this.uint = uint;
	}
	public void setPandorDoors(boolean pandorDoors) {
		this.pandorDoors = pandorDoors;
	}
	public String getFurniture() {
		return furniture;
	}
	public void setFurniture(String furniture) {
		this.furniture = furniture;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getExtras() {
		return extras;
	}
	public void setExtras(String extras) {
		this.extras = extras;
	}
	public String getGarden() {
		return garden;
	}
	public void setGarden(String garden) {
		this.garden = garden;
	}
	

}
