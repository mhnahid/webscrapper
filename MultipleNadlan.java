package webScrapping;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MultipleNadlan extends Thread {
	private Thread multipleNadlan;
	private String parentLink;
	private int startRow;
	private int endRow;
	private String threadName;
	
	MultipleNadlan(String parentLink, int startRow, int endRow)
	{
		this.parentLink=parentLink;
		this.startRow=startRow;
		this.endRow=endRow;
		
		threadName="MultipleNadlan  with range from "+startRow+" to  "+endRow; 
	}
	
	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (multipleNadlan == null)
		{
			multipleNadlan = new Thread (this, threadName);
			multipleNadlan.start ();
		}
	}
	
	private static void Println(Object object)
	{
		System.out.println(object);
	}
	
	@SuppressWarnings("null")
	public void run()
	{
		String parrenturl=parentLink;
		int pageCount=2;
		int exceptionPage=0;
		boolean contin=false;
		boolean willDestroy=false;
		int lastClickedRow=2;
		WebDriver driver =null;


		while(true)
		{
			if(willDestroy)
				break;
			
			Println("################################## While loop At Multiple Nadlan #########################################");
			try
			{
				if(driver!=null)
					driver.close();

				// Initialize WebDriver
				//driver = new FirefoxDriver();
				driver=Information.getDriver();

				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL
				driver.get(parrenturl);

				System.out.println("Running  Nad");

				// Maximize Window
				driver.manage().window().maximize();
				
				String pageUrl=driver.getCurrentUrl();
				if(pageUrl.equals(PathInfo.geolink))
				{
					Information.runExe(multipleNadlan);
					continue;
				}

				String parentHandle = driver.getWindowHandle();

				String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";

				List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
				System.out.println("Total number of rows :"+ rows.size());

				// clicking on each row
				if(rows.size()>0 && driver!=null){

					int checkBlank=0;
					int rowCount=startRow;
					if(pageCount==exceptionPage)
					{
						rowCount=lastClickedRow-1;
					}

					Println("Row count "+rowCount);

					for(;rowCount<=rows.size();rowCount++)
					{
						Instant start = Instant.now();

						if(checkBlank>6)
						{
							contin=true;
						}

						String publishedDate="";

						try
						{
							Nadlan nad= new Nadlan();

							String dynamicIdPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr["+rowCount+"]";

							String frqpath=dynamicIdPath+PathInfo._freqTdPrefix1+20+PathInfo._freqSuffix1+PathInfo._freqAn;
							String frqDate=dynamicIdPath+"/td[19]";


							System.out.println("Clicking on row  "+rowCount);

							List<WebElement> linkValue = driver.findElements(By.xpath(frqpath));

							// Checking if there is a link to click, cause there are many black rows
							if(linkValue.size()>0)
							{
								lastClickedRow=rowCount;
								
								List<WebElement> listDate = driver.findElements(By.xpath(frqDate));
								publishedDate=Information.retrieveDate(listDate);

								driver.findElement(By.xpath(frqpath)).click();

								boolean addFlag=false;

								// to check if there is an add page then close that page
								Println("Checking for add");

								for (String winHandle : driver.getWindowHandles())
								{
									driver.switchTo().window(winHandle);
									String newWindow=driver.getCurrentUrl();
									Println(winHandle+" it's url : "+newWindow);


									if(newWindow.equals(parrenturl))
									{
										Println("Continue");
										continue;
									}
									else
									{
										String [] linkArray=newWindow.split("/");

										for(int i=0;i<linkArray.length;i++)
										{
											if(linkArray[i].equals("Nadlan"))
											{
												addFlag=true;
												Println("valid page and Continue");
												break;
											}
										}
									}


									if(!addFlag)
									{
										Println("Wrong page !!!!!\n Closed!!!");
										driver.close();
										driver.switchTo().window(parentHandle);
										continue;
									}
								}

								Println(" Closing Checking for add"); // ending of closing page

								//Starting for getting data


								// Getting all new window
								boolean flag=false;
								int count=1;

								for (String winHandle : driver.getWindowHandles())
								{

									driver.switchTo().window(winHandle);
									String newWindow=driver.getCurrentUrl();

									Println("Page Count "+count+" New Window url : "+newWindow);

									if(newWindow.equals(parrenturl))
									{
										Println("Continue");
										count++;
										continue;
									}
									else
									{
										String [] linkArray=newWindow.split("/");

										for(int i=0;i<linkArray.length;i++)
										{
											if(linkArray[i].equals("Nadlan"))
											{
												flag=true;
												break;
											}
										}
									}

									if(flag==true)
									{
										Println("Found and Getting data");
										break;
									}
									else
									{
										Println("Wrong page !!!!! continue");
										count++;
										continue;
									}
								}

								Contact nContact= new Contact();
								nContact=Information.setContact(driver, Information.dataArray(driver, PathInfo.nadContactPath), PathInfo.nadPhonePath);

								if(nContact.getPhone1() !=null)
								{
									try
									{
										Println("Starting getting all data");
										String [] detailsArray=Information.dataArray(driver,PathInfo.nadDetailsPath);
										nad.setPubDate(publishedDate);
										nContact.setSettlement(Information.findData("ישוב",detailsArray));

										nad.setNadlanContact(nContact);

										nad.setPropertyType(Information.findData("סוג  הנכס",detailsArray));
										nad.setNeighborhood(Information.findData("שכונה",detailsArray));
										nad.setRooms(Information.findData("חדרים",detailsArray));
										nad.setFloor(Information.findData("קומה",detailsArray));
										nad.setBalconies(Information.findData("מרפסות", detailsArray));
										nad.setSize(Information.stringToInt(Information.findData("גודל במ\"ר",detailsArray)," "));
										
										//nad.setMoreDetails(Information.findData("מידע נוסף",detailsArray));
										
										nad.setAddress(Information.findData("כתובת",detailsArray));

										nad.setPrice(Information.getPrice(driver, PathInfo.yadPricePath));

										//nad.setFeatureDetails(Information.newLineRemover(driver, PathInfo.nadFeatureDetailsPath));
										
										nad.setAdNumber(Information.getAdNumber(driver));


										System.out.println("Object added");
										//FileWrite.NadlanCsvWrite(nad);
										pushToDB(nad);
									}
									catch(Exception ex)
									{
										Println(ex.getMessage());
										Println("Phone Couldn't retrived, adding to list");
										
										
									}

								}
								else {
									Println("Phone Couldn't retrived");
									
									if(!PathInfo.batchRun)
									{
										PathInfo.batchRun=true;
										Information.runExe(multipleNadlan);
										PathInfo.batchRun=false;
									}
									else
									{
										Thread.sleep(PathInfo.batchWait);
									}

								}
								checkBlank=0;

								driver.close(); // close newly opened window when done with it
								driver.switchTo().window(parentHandle); // switch back to the original window

								Instant end = Instant.now();
								Duration timeElapsed = Duration.between(start, end);
								Println(">>>>>>>>>>>>Nadlan Time taken: "+ timeElapsed.toMillis()/1000 +" seconds");

							}
							else
							{
								checkBlank++;
								contin=false;
							}

						}
						catch(Exception ex)
						{
							System.out.println(ex.getMessage());
							System.out.println(" #########################################\nException at Nadla\n#########################"+ex.getMessage());
							
							if(driver !=null)
							{
								driver.manage().deleteAllCookies();
								driver.close();
								driver.switchTo().window(parentHandle);
								if(driver != null)
								{
									driver.close();
								}
							}
							else
							{
								driver.switchTo().window(parentHandle);
								driver.manage().deleteAllCookies();
								driver.close();
							}
							
							
							//driver= new FirefoxDriver();
							driver=Information.getDriver();
							driver.get(parrenturl);// switch back to the original window
							//showMessage();
						}
						
						if(contin==true || rowCount==endRow)
						{
							contin=false;
							if(startRow>(rows.size()/2-1))
							{
								getTable2(driver,parrenturl,parentHandle);
								PathInfo.isNadlan2=true;
							}
							else
							{
								PathInfo.isNadlan1=true;
							}
							
							if(driver !=null)
							{
								driver.manage().deleteAllCookies();
								driver.close();
								if(driver != null)
								{
									driver.switchTo().window(parentHandle);
									driver.close();
								}
							}
							willDestroy=true;
							break;
							
						}
					}
				}
				else
				{
					Println("Conection Error");
				}

			}

			catch (Exception ex) {
				exceptionPage=pageCount;
				driver=null;
				System.out.println(ex.getMessage());
				Println("Conection Error");
				Information.runExe(multipleNadlan);
			}	
		}
	}


	private void getTable2(WebDriver driver,String parrenturl, String parentHandle)
	{


		String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/div[3]/table/tbody/tr";

		List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
		System.out.println("Total number of rows :"+ rows.size());

		// clicking on each row
		if(rows.size()>0){
			for(int rowCount=2;rowCount<=rows.size();rowCount++)
				//for(int rowCount=100;rowCount>1;rowCount--)
			{

				try
				{
					String dynamicIdPath="//*[@id=\"all_results_table\"]/div[4]/div[3]/table/tbody/tr["+rowCount+"]";
					List<WebElement> listWeb = driver.findElements(By.xpath(dynamicIdPath+"/td"));
					if(listWeb.size()>10)
					{


						Nadlan nad= new Nadlan();
						//String frqpath=dynamicIdPath+PathInfo._freqTdPrefix1+20+PathInfo._freqSuffix1+PathInfo._freqAn;
						String descriptionPath=dynamicIdPath+"/td[13]";
						String projectNamePath=dynamicIdPath+"/td[11]";
						String neighborHoodPath=dynamicIdPath+"/td[9]";
						String cityPath=dynamicIdPath+"/td[7]";


						System.out.println("Clicking on row  "+rowCount);

						List<WebElement> listDesc = driver.findElements(By.xpath(descriptionPath));
						List<WebElement> listprojectName = driver.findElements(By.xpath(projectNamePath));
						List<WebElement> listNeighborHood = driver.findElements(By.xpath(neighborHoodPath));
						List<WebElement> listCity = driver.findElements(By.xpath(cityPath));

						String moreDetails="Description : "+Information.retrieveDate(listDesc)+", ProjectName : "+Information.retrieveDate(listprojectName)+", Neighborhood : "+Information.retrieveDate(listNeighborHood)+", City : "+Information.retrieveDate(listCity);
						nad.setFeatureDetails(moreDetails);

						Println(moreDetails);


						// Checking if there is a link to click, cause there are many black rows
						int iRow=rowCount+1;
						String iframepath="//*[@id=\"all_results_table\"]/div[4]/div[3]/table/tbody/tr["+iRow+"]/td/div/div/iframe";

						driver.findElement(By.xpath(descriptionPath)).click();

						driver.switchTo().frame(driver.findElement(By.xpath(iframepath)));

						try
						{
							String phonePath="/html/body/div[1]/div[2]/div/div[1]/div[2]/div[1]/p[1]";
							String detailsPath="/html/body/div[1]/div[1]/div[1]/div[1]/div[2]";


							String [] detailsArray=Information.dataArray(driver,detailsPath);

							nad.setPropertyType(Information.findData("סוג  הנכס",detailsArray));
							nad.setNeighborhood(Information.findData("שכונה",detailsArray));
							nad.setRooms(Information.findData("חדרים",detailsArray));
							nad.setFloor(Information.findData("קומה",detailsArray));
							nad.setBalconies(Information.findData("מרפסות", detailsArray));
							nad.setSize(Information.stringToInt(Information.findData("גודל במ\"ר",detailsArray)," "));
							
							//nad.setMoreDetails(Information.findData("מידע נוסף",detailsArray));
							
							nad.setAddress(Information.findData("כתובת",detailsArray));



							Contact nContact= new Contact();
							List<WebElement>phoneList=driver.findElements(By.xpath(phonePath));

							nContact.setPhone1(Information.retrieveDate(phoneList));
							nad.setNadlanContact(nContact);
							Println(nContact.getPhone1());

							//FileWrite.NadlanCsvWrite(nad);
							pushToDB(nad);
						}catch(Exception ex)
						{
							Println(ex.getMessage());
						}

						driver.switchTo().defaultContent();
						driver.findElement(By.xpath(descriptionPath)).click();
					}
				}catch(Exception ex)
				{

					System.out.println(ex.getMessage());
					driver.manage().deleteAllCookies();
					driver.switchTo().window(parentHandle); // switch back to the original window
					//showMessage();
				}
			}
		}
		else{
			Println("Conection Error");
			//showConnectionError();
		}
	}

	private static void pushToDB(Nadlan nd) throws IOException 
	{
		Connection conn = null;
		try 
		{
			System.out.println("in pushDb");
			String current;

			current = new java.io.File( "." ).getCanonicalPath();
			System.out.println("Current dir:"+current);
			current.replace('\\', '/');

			conn = DriverManager.getConnection("jdbc:ucanaccess://"+current+"/Database3.mdb");
			Statement s = conn.createStatement();

			String query="INSERT INTO tbl_nadlan("
					+ "adNumber"
					+ ",phone"
					+",phone2"
					+",contactName"
					+ ",pubDate"
					+ ",settlement"
					+ ",neighborhood"
					+ ",address"
					+ ",rooms"
					+ ",floor"
					+ ",size"			// NUmber
					+ ",price"
					+ ",moreDetails)"

					+ "Values("
					+nd.getAdNumber()+","
					+ nd.getNadlanContact().getPhone1()
					+",'"+nd.getNadlanContact().getPhone2()+"'"
					+",'"+nd.getNadlanContact().getContactName()+"'"
					+",'"+nd.getPubDate()+"'"
					+",'"+nd.getNadlanContact().getSettlement()+"'"
					+",'"+nd.getNeighborhood()+"'"
					+",'"+nd.getAddress()+"'"
					+",'"+nd.getRooms()+"'"
					+",'"+nd.getFloor()+"'"
					+ ","+nd.getSize()			// Number
					+ ","+nd.getPrice()
					+",'"+nd.getFeatureDetails()+"')";
			System.out.println("execute query:"+query);
			s.executeUpdate(query);
			s.getConnection().commit();
			conn.close();
		} 
		catch (SQLException e) 
		{
			System.out.println(e.getMessage());

			//DATE 24-03-2016
			//Sharif - Big BUG was not to close DB connection ... don't leave any connection opened if crashes or throw exception
			if(conn != null)
				try {
					conn.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}

	}

}
