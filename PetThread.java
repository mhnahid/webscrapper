package webScrapping;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PetThread extends Thread {

	private Thread petT;
	private String threadName;
	PetThread(String name)
	{
		this.threadName=name;
	}

	//to Avoid System.out.. 

	private static void Println(Object object)
	{
		System.out.println(object);
	}


	public void run()
	{
		WebDriver driver =null;

		String parrenturl=PathInfo.linkPet;
		int pageCount=2;
		String newUrl="http://www.yad2.co.il/Pets/Pets.php?Page=";
		
		while(true)
		{

			Println("################################## While loop  Pet main thread #########################################");
			
			try
			{
				if(driver != null)
					driver.close();

				// Initialize WebDriver
//				 driver = new FirefoxDriver();
				driver=Information.getDriver();

				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL
				
				driver.get(parrenturl);
				//ArrayList<String> petList= new ArrayList<String>();

				System.out.println("Running  petMain");

				// Maximize Window
				driver.manage().window().maximize();

				String pageUrl=driver.getCurrentUrl();
				if(pageUrl.equals(PathInfo.geolink))
				{
					Information.runExe(petT);
					continue;
				}
				
				
					String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";
					
					List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
					System.out.println("Total number of rows :"+ rows.size());

					// clicking on each row
					if(rows.size()>0 && driver !=null)
					{
						MultiplePet pet1= new MultiplePet(parrenturl,2, rows.size()/2);
						
						MultiplePet pet2= new MultiplePet(parrenturl, rows.size()/2, rows.size());
					
						pet1.start();
						pet2.start();
						
						PathInfo.isPet1=false;
						PathInfo.isPet2=false;
						
						
						boolean isNextButtonAvailable=Information.isNextButton(driver);
						
						if(isNextButtonAvailable)
						{
							parrenturl=newUrl+pageCount;
							pageCount++;
						}
						else
						{
							parrenturl=PathInfo.linkPet;
							pageCount=2;
							Println("Starting againg from home page");
						}
						
						
						driver.close();

						while(true)
						{
							if(PathInfo.isPet1 && PathInfo.isPet2)
								break;
							Thread.sleep(PathInfo.mainThreadSleep);
						}
					
					
					
					}
					else
					{
						Println("Conection Error");
					}
				
			
			}
			catch(Exception ex)
			{
				driver=null;
				System.out.println(ex.getMessage());
				Println("Conection Error");
				Information.runExe(petT);
			}
		}
	}

	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (petT == null)
		{
			petT = new Thread (this, threadName);
			petT.start ();
		}
	}
	
	
	

}
