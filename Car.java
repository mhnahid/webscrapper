package webScrapping;

public class Car {
	private Contact carContact;
	private String manufacturer;
	private String Model;
	private String subModel;
	private int volume;
	private int year;
	private String h_AliihRoad;
	private int hand;
	private double Price;
	private long mileage;
	private String description;
	private String pubDate;
	private String automation;
	
	int adNumber;
	
	public int getAdNumber() {
		return adNumber;
	}
	public void setAdNumber(int adNumber) {
		this.adNumber = adNumber;
	}
	public String getAutomation() {
		
		if(automation!=null)
			return Utils.fixToSql(this.automation);
			else return null;
	}
	public void setAutomation(String automation) {
		this.automation = automation;
	}
	public String getPubDate() {

		if(pubDate!=null)
			return Utils.fixToSql(this.pubDate);
			else return null;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public String getModel() {
		return Model;
	}
	public void setModel(String model) {
		Model = model;
	}
	public Contact getCarContact() {
		return carContact;
	}
	public void setCarContact(Contact carContact) {
		this.carContact = carContact;
	}
	public String getManufacturer() {
		if(manufacturer!=null)
			return Utils.fixToSql(this.manufacturer);
			else return null;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getSubModel() {
		if(subModel!=null)
			return Utils.fixToSql(this.subModel);
			else return null;
	}
	public void setSubModel(String subModel) {
		this.subModel = subModel;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getH_AliihRoad() {
		if(h_AliihRoad!=null)
			return Utils.fixToSql(this.h_AliihRoad);
			else return null;
	}
	public void setH_AliihRoad(String h_AliihRoad) {
		this.h_AliihRoad = h_AliihRoad;
	}
	public int getHand() {
		return hand;
	}
	public void setHand(int hand) {
		this.hand = hand;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}
	public long getMileage() {
		return mileage;
	}
	public void setMileage(long mileage) {
		this.mileage = mileage;
	}
	public String getDescription() {
		if(description!=null)
			return Utils.fixToSql(this.description);
			else return null;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
