package webScrapping;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MultiplePet  extends Thread {
	private Thread multiplePet;
	private String parentLink;
	private int startRow;
	private int endRow;
	private String threadName;
	
	MultiplePet(String parentLink, int startRow, int endRow)
	{
		this.parentLink=parentLink;
		this.startRow=startRow;
		this.endRow=endRow;
		
		threadName="MultiplePet with range from "+startRow+" to  "+endRow; 
	}
	
	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (multiplePet == null)
		{
			multiplePet = new Thread (this, threadName);
			multiplePet.start ();
		}
	}
	
	private static void Println(Object object)
	{
		System.out.println(object);
	}

	
	@SuppressWarnings("null")
	public void run()
	{
		String parrenturl=parentLink;
		int pageCount=2;
		int exceptionPage=0;
		boolean willDestroy=false;
		int lastClickedRow=startRow;
		WebDriver driver =null;

		
		while(true)
		{

			if(willDestroy)
				break;
			
			Println("################################## While loop Multiple Pet #########################################");
			try
			{
				if(driver != null)
					driver.close();

				// Initialize WebDriver
				 //driver = new FirefoxDriver();
				driver=Information.getDriver();

				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL
				
				driver.get(parrenturl);
				//ArrayList<String> petList= new ArrayList<String>();

				System.out.println("Running ");

				// Maximize Window
				driver.manage().window().maximize();

				String parentHandle = driver.getWindowHandle();
				
				String pageUrl=driver.getCurrentUrl();
				if(pageUrl.equals(PathInfo.geolink))
				{
					Information.runExe(multiplePet);
					continue;
				}
				
				
					String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";
					
					List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
					System.out.println("Total number of rows :"+ rows.size());

					// clicking on each row
					if(rows.size()>0 && driver !=null)
					{
						int rowCount=startRow;
						if(pageCount==exceptionPage)
						{
							rowCount=lastClickedRow-1;
						}
						
						Println("Row count "+rowCount);
							
						for(;rowCount<=rows.size();rowCount++)
							//for(int rowCount=100;rowCount>1;rowCount--)
						{
							Instant start = Instant.now();
							String publishedDate="";
							try
							{
								Pet pet=new Pet();

								String dynamicIdPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr["+rowCount+"]";

								//String frqpath="/html/body/div[5]/div[16]/div[1]/div[3]/div[1]/div[4]/table/tbody/tr["+rowCount+"]/td[22]/a";

								String frqpath=dynamicIdPath+PathInfo._freqTdPrefix1+18+PathInfo._freqSuffix1+PathInfo._freqAn;
								String frqDate=dynamicIdPath+"/td[17]";


								System.out.println("Clicking on row  "+rowCount);

								List<WebElement> linkValue = driver.findElements(By.xpath(frqpath));

								// Checking if there is a link to click, cause there are many black rows
								
								if(linkValue.size()>0)
								
								{
									lastClickedRow=rowCount;
									List<WebElement> listDate = driver.findElements(By.xpath(frqDate));
									publishedDate=Information.retrieveDate(listDate);

									driver.findElement(By.xpath(frqpath)).click();

									boolean addFlag=false;

									// to check if there is an add page then close that page
									Println("Checking for add");

									for (String winHandle : driver.getWindowHandles())
									{
										driver.switchTo().window(winHandle);
										String newWindow=driver.getCurrentUrl();
										Println(winHandle+" it's url : "+newWindow);


										if(newWindow.equals(parrenturl))
										{
											Println("Continue");
											continue;
										}
										else
										{
											String [] linkArray=newWindow.split("/");

											for(int i=0;i<linkArray.length;i++)
											{
												if(linkArray[i].equals("Pets"))
												{
													addFlag=true;
													Println("valid page and Continue");
													break;
												}
											}
										}


										if(!addFlag)
										{
											Println("Wrong page !!!!!\n Closed!!!");
											driver.close();
											driver.switchTo().window(parentHandle);
											continue;
										}
									}

									Println(" Closing Checking for add"); // ending of closing page

									boolean flag=false;
									int count=1;

									for (String winHandle : driver.getWindowHandles())
									{

										driver.switchTo().window(winHandle);
										String newWindow=driver.getCurrentUrl();

										Println("Page Count "+count+" New Window url : "+newWindow);

										if(newWindow.equals(parrenturl))
										{
											Println("Continue");
											count++;
											continue;
										}
										else
										{
											String [] linkArray=newWindow.split("/");

											for(int i=0;i<linkArray.length;i++)
											{
												if(linkArray[i].equals("Pets"))
												{
													flag=true;
													break;
												}
											}
										}

										if(flag==true)
										{
											Println("Found and Getting data");
											break;
										}
										else
										{
											Println("Wrong page !!!!! continue");
											count++;
											continue;
										}
									}

									// pet Contact
									Contact pContact= new Contact();
				
									pContact=Information.setContact(driver, Information.dataArray(driver, PathInfo.petContactPath), PathInfo.petPhonePath);
									
									if(pContact.getPhone1() !=null)
									{
										try
										{
											Println("Starting getting all data");
											String [] detailsArray=Information.dataArray(driver, PathInfo.petDetailsPath);
											pet.setPubDate(publishedDate);
											
											pet.setPetContact(pContact);
											
											pet.setAnimalName(Information.findData("בעל חיים", detailsArray));
											pet.setType(Information.findData("סוג", detailsArray));
											pet.setAction(Information.findData("פעולה", detailsArray));
											
											Println("At middle");
											//pet.setAge(Information.stringToInt(Information.findData("גיל", detailsArray), " "));
											String age=Information.findData("גיל", detailsArray);
											
											pet.setAge(Utils.onlyNumbers(age));
											pet.setSex(Information.findData("מין", detailsArray));
											Println("before ders");
											pet.setDogBreeds(Information.findData("כלב גזעי", detailsArray));
											pet.setPrice(Information.getPrice(driver, PathInfo.petPricePath));
											//pet.setPetMainDetails(Information.newLineRemover(driver, PathInfo.petMoreDetailsPath));
											pet.setAdNumber(Information.getAdNumber(driver));

											
											System.out.println("Object added");
											FileWrite.petCsvWrite(pet);
											pushToDB(pet);
										}
										catch(Exception ex)
										{
											Println("<<<<<<<<<PetPhone Exception >>>>>>>>> \n"+ex.getMessage());
											Println("Phone Couldn't retrived, adding to list");
										}
										
									}
									else
									{
										//String currentLink=driver.getCurrentUrl()+"^"+publishedDate;
										//petList.add(currentLink);
										Println("Phone Couldn't retrived, adding to list");
										
										if(!PathInfo.batchRun)
										{
											PathInfo.batchRun=true;
											Information.runExe(multiplePet);
											PathInfo.batchRun=false;
										}
										else
										{
											Thread.sleep(PathInfo.batchWait);
										}

									}

									driver.close(); // close newly opened window when done with it
									driver.switchTo().window(parentHandle); // switch back to the original window
									
									Instant end = Instant.now();
									Duration timeElapsed = Duration.between(start, end);
									Println(">>>>>>>>>>>>Pet Time taken: "+ timeElapsed.toMillis()/1000 +" seconds");
								}

							}catch(Exception ex)
							{
								//String currentLink=driver.getCurrentUrl()+" ^"+publishedDate;
								//petList.add(currentLink);

								System.out.println(ex.getMessage());
								System.out.println(" #########################################\nException at Pet\n#########################"+ex.getMessage());
								if(driver !=null)
								{
									driver.manage().deleteAllCookies();
									driver.close();
									driver.switchTo().window(parentHandle);
									if(driver != null)
									{
										driver.close();
									}
								}
								else
								{
									driver.switchTo().window(parentHandle);
									driver.manage().deleteAllCookies();
									driver.close();
								}
								// close newly opened window when done with it
								//
								//driver= new FirefoxDriver();
								driver=Information.getDriver();
								driver.get(parrenturl);
							}
							
							if(rowCount==endRow)
							{
								if(startRow>(rows.size()/2-1))
								{
									PathInfo.isPet2=true;
								}
								else
								{
									PathInfo.isPet1=true;
								}
								if(driver !=null)
								{
									driver.manage().deleteAllCookies();
									driver.close();
									if(driver != null)
									{
										driver.switchTo().window(parentHandle);
										driver.close();
									}
								}
								willDestroy=true;
								break;
							}
						}
					}
					else
					{
						Println("Conection Error");
					}
				
			
			}
			catch(Exception ex)
			{
				exceptionPage=pageCount;
				driver=null;
				System.out.println(ex.getMessage());
				Println("Conection Error");
				Information.runExe(multiplePet);
			}
		}
	}
	
	private void pushToDB(Pet pet) throws IOException 
	{
		Connection conn = null;
		try 
		{
			System.out.println("in pushDb");
			String current;

			current = new java.io.File( "." ).getCanonicalPath();
			System.out.println("Current dir:"+current);
			current.replace('\\', '/');

			conn = DriverManager.getConnection("jdbc:ucanaccess://"+current+"/Database3.mdb");
			Statement s = conn.createStatement();

			String query="INSERT INTO tbl_pet("
					+ "adNumber"
					+ ",phone1"
					+ ",phone2"
					+ ",contactName"
					+ ",pubDate"
					+ ",salesArea"
					+ ",settlement"
					+ ",animalName"
					+ ",type"			// NUmber
					+ ",action"
					+ ",age"
					+ ",sex"
					+",dogBreeds"
					+",price"
					+",petMoreDetails)"
					+ "Values("
					+pet.getAdNumber()+",'"
					+pet.getPetContact().getPhone1()+"','"
					+pet.getPetContact().getPhone2()+"','"
					+pet.getPetContact().getContactName()+"','"
					+pet.getPubDate()+"','"
					+pet.getPetContact().getSalesArea()+"','"
					+pet.getPetContact().getSettlement()+"','"
					+pet.getAnimalName()+"','"
					+pet.getType()+"','"
					+pet.getAction()+"',"+
					pet.getAge()+",'"+
					pet.getSex()+"','"+
					pet.getDogBreeds()+"',"+
					pet.getPrice()+",'"
					+pet.getPetMainDetails()+"')";
			System.out.println("execute query:"+query);
			s.executeUpdate(query);
			s.getConnection().commit();
			conn.close();
		} 
		catch (SQLException e) 
		{
			System.out.println(e.getMessage());

			//DATE 24-03-2016
			//Sharif - Big BUG was not to close DB connection ... don't leave any connection opened if crashes or throw exception
			if(conn != null)
				try {
					conn.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}

	}
}
