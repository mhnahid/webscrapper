package webScrapping;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MultipleCar extends Thread {
	private Thread multipleCar;
	private String parentLink;
	private int startRow;
	private int endRow;
	private String threadName;

	MultipleCar(String parentLink, int startRow, int endRow)
	{
		this.parentLink=parentLink;
		this.startRow=startRow;
		this.endRow=endRow;

		threadName="Car with range from "+startRow+" to  "+endRow; 
	}

	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (multipleCar == null)
		{
			multipleCar = new Thread (this, threadName);
			multipleCar.start ();
		}
	}

	private static void Println(Object object)
	{
		System.out.println(object);
	}


	@SuppressWarnings("null")
	public void run()
	{
		String parrenturl=parentLink;
		int pageCount=2;
		int exceptionPage=0;
		boolean willDestroy=false;
		WebDriver driver=null;
		int lastClickedRow=startRow;
		String parentHandle=""; 


		while(true)
		{

			if(willDestroy)
			{
				Println("Out of multiple thread");
				break;
			}

			Println("################################## While loop at multiple Car #########################################\n"+threadName);
			try
			{
				if(driver != null)
					driver.close();

				//driver= new FirefoxDriver();
				driver=Information.getDriver();
				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL
				driver.get(parrenturl);

				// Maximize Window
				driver.manage().window().maximize();

				parentHandle = driver.getWindowHandle();

				if(driver.getCurrentUrl()==" http://geo.yad2.co.il/")
				{
					Information.runExe(multipleCar);
					continue;
				}

				//ArrayList<String> carList= new ArrayList<String>();

				String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";

				List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
				System.out.println("Total number of rows :"+ rows.size());

				// clicking on each row
				if(rows.size()>0 && driver!=null){
					int rowCount=startRow;

					if(pageCount==exceptionPage)
					{
						rowCount=lastClickedRow;
					}
					Println("Row Count "+rowCount);



					for(;rowCount<=endRow;rowCount++)
					{
						Instant start = Instant.now();
						String publishedDate="";

						try
						{
							Car spCar=new Car();

							String dynamicIdPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr["+rowCount+"]";

							String frqpath=dynamicIdPath+"/td[22]/a";
							String frqDate=dynamicIdPath+"/td[21]";


							System.out.println("Clicking on row  "+rowCount+" of thread "+threadName);

							List<WebElement> linkValue = driver.findElements(By.xpath(frqpath));


							// Checking if there is a link to click, cause there are many black rows
							if(linkValue.size()>0)
							{
								lastClickedRow=rowCount;

								List<WebElement> listDate = driver.findElements(By.xpath(frqDate));
								publishedDate=Information.retrieveDate(listDate);




								Println(" Date "+publishedDate);

								driver.findElement(By.xpath(frqpath)).click();

								// to check if there is an add page then close that page

								boolean addFlag=false;
								Println("Checking for add");

								for (String winHandle : driver.getWindowHandles())
								{

									driver.switchTo().window(winHandle);
									String newWindow=driver.getCurrentUrl();
									Println(winHandle+" it's url : "+newWindow);


									if(newWindow.equals(parrenturl))
									{
										Println("Continue");
										continue;
									}
									else
									{
										String [] linkArray=newWindow.split("/");

										for(int i=0;i<linkArray.length;i++)
										{
											if(linkArray[i].equals("Cars"))
											{
												addFlag=true;
												Println("valid page and Continue");
												break;
											}
										}
									}


									if(!addFlag)
									{
										Println("Wrong page !!!!!\n Closed!!!");
										driver.close();
										driver.switchTo().window(parentHandle);
										continue;
									}
								}

								Println(" Closing Checking for add"); // ending of closing page

								//Starting for getting data


								// Getting all new window
								boolean flag=false;
								int count=1;

								for (String winHandle : driver.getWindowHandles())
								{

									driver.switchTo().window(winHandle);
									String newWindow=driver.getCurrentUrl();

									Println("Page Count "+count+" New Window url : "+newWindow);

									if(newWindow.equals(parrenturl))
									{
										Println("Continue");
										count++;
										continue;
									}
									else
									{
										String [] linkArray=newWindow.split("/");

										for(int i=0;i<linkArray.length;i++)
										{
											if(linkArray[i].equals("Cars"))
											{
												flag=true;
												break;
											}
										}
									}

									if(flag==true)
									{
										Println("Found and Getting data");
										break;
									}
									else
									{
										Println("Wrong page !!!!! continue");
										count++;
										continue;
									}
								}
								
								Contact cContact= new Contact();
								cContact=Information.setContact(driver, Information.dataArray(driver, PathInfo.carContactPath), PathInfo.carPhonePath);

								if(cContact.getPhone1() !=null)
								{
									try
									{
										Println("Starting getting all data");
										String [] detailsArray=Information.dataArray(driver, PathInfo.carDetailsPath);
										spCar.setPubDate(publishedDate);

										spCar.setCarContact(cContact);

										spCar.setManufacturer(Information.findData("יצרן", detailsArray));
										spCar.setModel(Information.findData("דגם", detailsArray));
										spCar.setSubModel(Information.findData("תת דגם", detailsArray));
										spCar.setVolume(Information.stringToInt(Information.findData("נפח", detailsArray), ","));
										spCar.setYear(Information.stringToInt(Information.findData("שנה", detailsArray), " "));

										spCar.setH_AliihRoad(Information.findData("ח.עלייה לכביש", detailsArray));

										//spCar.setHand(Information.stringToInt(Information.findData("ייד:", detailsArray), " "));
										spCar.setAutomation(Information.findData("ת. הילוכים", detailsArray));
										//spCar.setDescription(Information.newLineRemover(driver, PathInfo.carDescPath));

										spCar.setPrice(Information.getPrice(driver, PathInfo.carPricePath));

										String [] mileageArrya=Information.dataArray(driver, PathInfo.carMoreDetailsPath);
										spCar.setMileage(Information.stringToInt(Information.findData("ק\"מ",mileageArrya), ","));

										spCar.setAdNumber(Information.getAdNumber(driver));

										System.out.println("Object added");
										FileWrite.CarCsvWrite(spCar);
										pushToDB(spCar);
									}
									catch(Exception ex)
									{
										Println(ex.getMessage());
										Println("Phone Couldn't retrived");
									}

								}
								else
								{
									//String currentLink=driver.getCurrentUrl()+"^"+publishedDate;
									//carList.add(currentLink);
									Println("Phone Couldn't retrived");

									if(!PathInfo.batchRun)
									{

										PathInfo.batchRun=true;
										Information.runExe(multipleCar);
										PathInfo.batchRun=false;
									}
									else
									{
										Thread.sleep(PathInfo.batchWait);
									}
								}


								driver.close(); // close newly opened window when done with it
								driver.switchTo().window(parentHandle); // switch back to the original window

								Instant end = Instant.now();
								Duration timeElapsed = Duration.between(start, end);
								Println(">>>>>>>>>>>> Car Time taken: "+ timeElapsed.toMillis()/1000 +" seconds");

							}

						}catch(Exception ex)
						{
							System.out.println(threadName+" \n#########################################\nException at Car\n#########################"+ex.getMessage());
							if(driver !=null)
							{
								driver.manage().deleteAllCookies();
								driver.close();
								driver.switchTo().window(parentHandle);
								if(driver != null)
								{
									driver.close();
								}
							}
							else
							{
								driver.switchTo().window(parentHandle);
								driver.manage().deleteAllCookies();
								driver.close();
							}
							// close newly opened window when done with it
							//
							//driver= new FirefoxDriver();
							driver=Information.getDriver();
							driver.get(parrenturl);
						}

						if(rowCount==endRow)
						{
							Println("At end of page row"+threadName);
							if(startRow>23)
								PathInfo.isCar2=true;
							else
								PathInfo.isCar1=true;

							driver.manage().deleteAllCookies();
							driver.close();
							if(driver != null)
							{
								driver.switchTo().window(parentHandle);
								driver.close();
							}
							willDestroy=true;
							break;
						}

					}
				}else
				{
					//showConnectionError();
					Println("Conection Error");
				}

			}
			catch(Exception ex)
			{
				exceptionPage=pageCount;
				driver=null;

				System.out.println(ex.getMessage());
				Println("Conection Error " +threadName);
				Information.runExe(multipleCar);
				//showConnectionError();
			}

		}
	}


	private void pushToDB(Car car) throws IOException 
	{
		Connection conn = null;
		try 
		{
			System.out.println("in pushDb");
			String current;

			current = new java.io.File( "." ).getCanonicalPath();
			System.out.println("Current dir:"+current);
			current.replace('\\', '/');

			conn = DriverManager.getConnection("jdbc:ucanaccess://"+current+"/Database3.mdb");
			Statement s = conn.createStatement();

			String query="INSERT INTO tbl_car("
					+ "AdNumber"
					+ ",phone1"
					+ ",phone2"
					+ ",contactName"
					+ ",pubDate"
					+ ",salesArea"
					+ ",settlement"
					+ ",manufacturer"
					+ ",model"			// NUmber
					+ ",subModel"
					+ ",volume"
					+ ",year"
					+ ",monthOfGettingRoad"
					+ ",hand"
					+ ",Price"
					+ ",mileage"
					+ ",description)"

					+ "Values("
					+car.getAdNumber()+",'"
					+car.getCarContact().getPhone1()+"','"
					+car.getCarContact().getPhone2()+"','"
					+car.getCarContact().getContactName()+"','"
					+car.getPubDate()+"','"
					+car.getCarContact().getSalesArea()+"','"
					+car.getCarContact().getSettlement()+"','"
					+car.getManufacturer()+"','"
					+car.getModel()+"','"
					+car.getSubModel()+"',"+
					car.getVolume()+","+
					car.getYear()+",'"+
					car.getH_AliihRoad()+"',"
					+car.getHand()+","
					+car.getPrice()+","
					+car.getMileage()+",'"
					+car.getDescription()+"')";
			System.out.println("execute query:"+query);
			s.executeUpdate(query);
			s.getConnection().commit();
			conn.close();
		} 
		catch (SQLException e) 
		{
			System.out.println(e.getMessage());

			//DATE 24-03-2016
			//Sharif - Big BUG was not to close DB connection ... don't leave any connection opened if crashes or throw exception
			if(conn != null)
				try {
					conn.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}

	}



}
