package webScrapping;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Contact {
	private String contactName;
	private String salesArea;
	private String settlement;
	private String phone1;
	private String phone2;


	private static void Println(Object object)
	{
		System.out.println(object);
	}

	public void setContactName(String contact)
	{
		this.contactName=contact;
	}

	public String getContactName()
	{
		if(contactName!=null)
			return Utils.fixToSql(contactName);
			else return null;
	}

	public void setSalesArea(String sales_area)
	{
		this.salesArea=sales_area;
	}

	public String getSalesArea()
	{
		if(salesArea!=null)
			return Utils.fixToSql(salesArea);
			else return null;
	}

	public void setSettlement(String settlement)
	{
		this.settlement=settlement;
	}

	public String getSettlement()
	{
		if(settlement!=null)
			return Utils.fixToSql(settlement);
			else return null;
	}

	public void setPhone1(String phoneNbr1)
	{
		this.phone1=phoneNbr1;
	}

	public String getPhone1()
	{
		if(phone1!=null)
			return Utils.fixToSql(phone1);
			else return null;
	}

	public void setPhone2(String phoneNbr2)
	{
		this.phone2=phoneNbr2;
	}

	public String getPhone2()
	{
		if(phone2!=null)
			return Utils.fixToSql(phone2);
			else return null;
	}


	static String justNumber(WebDriver driver, String locPath)
	{
		String phnNumber="";
		try
		{
			Println("At  justNumber");
			String newPath=locPath;
			List<WebElement> drop = driver.findElements(By.xpath(newPath));
			Iterator<WebElement> i = drop.iterator();

			String phoneNumber="";
			while(i.hasNext()) {
				
				WebElement row = i.next();
				//System.out.println("printing web element");
				 phoneNumber= row.getText();
				if(phoneNumber.length()>3)
				{
					phnNumber+=phoneNumber+" # ";
					Println("At just "+phnNumber);
				}
			}
		}catch(Exception ex)
		{
			phnNumber=null;
			Println("At justNumber exception"+ex.getMessage());
		}
		Println("At justNumber value "+phnNumber);
		return phnNumber;
	}
	
	public static String getPhnNumber(WebDriver driver,String locPath)
	{
		String phnNumber="";
		try
		{
			System.out.println("starting getPhnNumber ");
			String newTabXpath="//*[@id=\"toShowPhone\"]/a";
			driver.findElement(By.xpath(newTabXpath)).click();

			//String newPath="/html/body/div[5]/div[13]/div/div/div/table/tbody/tr[1]/td[1]/div/table/tbody/tr/td[2]/div/div[2]/div[2]/table[2]/tbody/tr/td/table/tbody/tr/td[2]/b";

			phnNumber=justNumber(driver, locPath);
//			List<WebElement> drop = driver.findElements(By.xpath(newPath));
//			Iterator<WebElement> i = drop.iterator();
//
//			String phoneNumber="";
//			while(i.hasNext()) {
//				
//				WebElement row = i.next();
//				//System.out.println("printing web element");
//				 phoneNumber= row.getText();
//				 if(phoneNumber.length()>3)
//					{
//						phnNumber+=phoneNumber+" # ";
//						Println("At just "+phnNumber);
//					}
//				//Println("phoneNumber : "+phnNumber);
//			}
		}catch(Exception ex)
		{
			phnNumber=null;
			Println("At Contact.getphoneNUmber exception "+ex.getMessage());
		}
		finally
		{
			Println("At finally");
			if(phnNumber==null)
			{
				Information.captchCracker(driver);
				phnNumber=justNumber(driver, locPath);
				Println("At Contact.getphoneNUmber Final "+phnNumber);
			}
		}
		Println("phoneNumber : "+phnNumber);
		return phnNumber;
	}
}
