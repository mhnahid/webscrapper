package webScrapping;

public class Utils {

	static String fixToSql(String str){
		if(str!=null){
			str=str.replaceAll("\'", "").replaceAll(",", "").replaceAll("\"", "");
			return str;
		}
		else return null;
	}

	static int onlyNumbers(String str){
		if(str!=null&&str.matches(".*\\d+.*")){
			int toReturn=Integer.parseInt(str.replaceAll("[^0-9.]", ""));
		return toReturn;
		}
		else{
			return -1;
		}
	}
		static void printArray(Object[] objects){
		if(objects.length>=1){
		System.out.println("[");
		for(int i=0;i<objects.length-1;i++)System.out.print(objects[i]+",");
		System.out.print(objects[objects.length-1]+"]");
		}
		else System.out.println("Array is empty.");
	}
}
