package webScrapping;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class YadThread  extends Thread {


	private Thread yadT;
	private String threadName;

	YadThread(String name)
	{
		this.threadName=name;
	}

	private static void Println(Object object)
	{
		System.out.println(object);
	}

	public void run()
	{
		WebDriver driver =null;

		String parrenturl=PathInfo.linkYad;
		int pageCount=2;
		String newUrl="http://www.yad2.co.il/Yad2/Yad2.php?Page=";


		while(true)
		{


			Println("################################## While loop  main thread Yad #########################################");

			try
			{
				if(driver != null)
					driver.close();

				// Initialize WebDriver
				//driver = new FirefoxDriver();
				//driver= new ChromeDriver();
				driver=Information.getDriver();
				//driver = new RemoteWebDriver("http://127.0.0.1:9515", DesiredCapabilities.chrome());


				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL
				driver.get(parrenturl);

				System.out.println("Running  Yad main thread");

				// Maximize Window
				driver.manage().window().maximize();	

				// checking for http://geo.yad2.co.il/
				String pageUrl=driver.getCurrentUrl();
				if(pageUrl.equals(PathInfo.geolink))
				{
					Information.runExe(yadT);
					continue;
				}

				String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";

				List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
				System.out.println("Total number of rows :"+ rows.size());

				// clicking on each row
				if(rows.size()>0 && driver !=null)
				{
					MultipleYad yad1= new MultipleYad(parrenturl, 2, rows.size()/2);

					MultipleYad yad2= new MultipleYad(parrenturl, rows.size()/2,rows.size());

					yad1.start();
					yad2.start();

					PathInfo.isYad1=false;
					PathInfo.isYad2=false;
					
					
					
					boolean isNextButtonAvailable=Information.isNextButton(driver);

					if(isNextButtonAvailable)
					{
						parrenturl=newUrl+pageCount;
						pageCount++;
					}
					else
					{
						parrenturl=PathInfo.linkYad;
						pageCount=2;
						Println("Starting againg from home page");
					}

					driver.close();

					while(true)
					{
						if(PathInfo.isYad1 && PathInfo.isYad2)
							break;
						Thread.sleep(PathInfo.mainThreadSleep);
					}

				}
				else
				{
					Println("Conection Error");
				}


			}
			catch(Exception ex){
				driver=null;
				System.out.println(ex.getMessage());
				Println("Conection Error");
				Information.runExe(yadT);
			}
		}
	}


	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (yadT == null)
		{
			yadT = new Thread (this, threadName);
			yadT.start ();
		}
	}

}
