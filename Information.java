package webScrapping;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;



public class Information {


	private static void Println(Object object)
	{
		System.out.println(object);
	}

	public static int getAdNumber(WebDriver driver){
		String adNumber=driver.findElement(By.className("adNumber")).getText();
		return Utils.onlyNumbers(adNumber);
	}

	// Matching data 
	public static String findData(String string, String[] dataArray) {
		Println("Starting findData for "+string );
		for(String line:dataArray){
			if(line.contains(string)){
				String toReturn= line.replaceAll(string+":", "");
				return toReturn;
			}
		}
		return null;
	}

	// removing "\n" from description
	public static String newLineRemover(WebDriver driver,String path)
	{
		try
		{
			System.out.println("starting new Line Remover ");

			List<WebElement> drop = driver.findElements(By.xpath(path));
			Iterator<WebElement> i = drop.iterator();

			String desc="";
			while(i.hasNext()) {
				WebElement row = i.next();
				String tmp=row.getText();
				tmp=tmp.replace("\n", " ");
				desc +=tmp.trim();
			}

			Println("getDesCription : "+desc);
			return desc;
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			return null;
		}
	}

	// getting an array from weblist
	public static String [] dataArray(WebDriver driver, String Path)
	{
		Println("Starting Data Array maker");
		try
		{
			String tmpDate="";
			List<WebElement>data=driver.findElements(By.xpath(Path));
			Iterator<WebElement> i = data.iterator();

			WebElement row = i.next();
			tmpDate += row.getText();
			String [] dataArray=tmpDate.split("\n");

			return dataArray;
		}
		catch(Exception ex)
		{
			Println(ex.getMessage());
			return null;
		}
	}

	public static String getPhnNumber(WebDriver driver,String phonePath)
	{
		Println("Starting Phone Number");
		String phnNumber="";
		try
		{
			System.out.println("starting getPhnNumber ");
			String newTabXpath="//*[@id=\"toShowPhone\"]/a";
			driver.findElement(By.xpath(newTabXpath)).click();

			List<WebElement> drop = driver.findElements(By.xpath(phonePath));
			Iterator<WebElement> i = drop.iterator();

			String phoneNumber="";
			while(i.hasNext()) {

				WebElement row = i.next();
				//System.out.println("printing web element");
				phoneNumber= row.getText();
				if(phoneNumber.length()>3)
					phnNumber+=phoneNumber+" # ";
			}
		}catch(Exception ex)
		{
			phnNumber=null;
			//phnNumber=captchCracker(driver);
			Println("after calling cracker\n"+ex.getMessage());

		}
		if(phnNumber.length()<1)
			//phnNumber=captchCracker(driver);

			Println("phoneNumber : "+phnNumber);
		return phnNumber;
	}

	// this is being called from app to capture the screen shot
	public static void captchCracker(WebDriver driver)
	{
		try {
			Println("At Captcha cracker");

			WebElement newElement=driver.findElement(By.id("captch_frame"));//xpath("//*[@id=\"captch_frame\"]"));

			Point point = newElement.getLocation();
			int xcord = point.getX();
			int ycord = point.getY();


			//js.executeScript("$('.captchaReload').hide()");

			String parentHandle = driver.getWindowHandle();

			driver.switchTo().frame(driver.findElement(By.id("captch_frame")));//xpath("//*[@id=\"captch_frame\"]")));  $(".divIDClass").hide();

			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("document.querySelector(\".captchaReload\").style.display = 'none'");

			WebElement Image = driver.findElement(By.className("captcha"));
			captureElementScreenshot(Image,driver,xcord,ycord, parentHandle);
		}
		catch (IOException e)
		{
			Println("At captch exception");
			e.printStackTrace();
		}
		return;
	}

	// here we are taking screen shot and send to decode
	public static void captureElementScreenshot(WebElement element,WebDriver driver,int x,int y,String parentHandle) throws IOException
	{ 
		try
		{
			Println("At file Image");
			File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

			//Used selenium getSize() method to get height and width of element. //Retrieve width of element. 
			//int ImageWidth = element.getSize().getWidth(); 

			//Retrieve height of element.
			//int ImageHeight = element.getSize().getHeight(); 


			//Used selenium Point class to get x y coordinates of Image element. //get location(x y coordinates) of the element. 
			Point point = element.getLocation();
			int xcord = point.getX()+x;
			int ycord = point.getY()+y;

			Println("x: "+xcord+ "  y : "+ycord);

			//Reading full image screenshot. 
			BufferedImage img = ImageIO.read(screen); 

			//cut Image using height, width and x y coordinates parameters. 
			BufferedImage dest = img.getSubimage(xcord, ycord, 250, 150); 
			ImageIO.write(dest, "png", screen); //Used FileUtils class of apache.commons.io. //save Image screenshot In D: drive. 


			String fileName = new SimpleDateFormat("yyyyMMddhhmmss'.png'").format(new Date());
			//String filePath="D:\\screenshot"+fileName;
			File file= new File(fileName);

			FileUtils.copyFile(screen,file);
			String capValue=Information.readCaptcha(fileName);
			if(capValue!=null)
			{
				driver.findElement(By.xpath("//*[@id=\"captcha_input\"]")).sendKeys(capValue);
				driver.findElement(By.className("captchaSubmit")).click();//xpath =/html/body/div/div[1]/form/input[2]

			}
			file.delete();
			driver.switchTo().window(parentHandle);
			return;
		}
		catch(Exception e)
		{
			Println("At captureElementScreenshot exception");
			e.printStackTrace();
		}
	}

	public static Contact setContact(WebDriver driver, String [] dataArray, String phonePath)
	{
		Println("Starting Contact");
		Contact contact= new Contact();

		String phoneNumber=Contact.getPhnNumber(driver,phonePath);

		if(phoneNumber.length()<3)
			phoneNumber=Contact.getPhnNumber(driver,phonePath);

		if( phoneNumber !=null)
		{
			if(!phoneNumber.isEmpty())
			{
				if(phoneNumber.length()>8)
				{
					String [] phnList= phoneNumber.split("#");
					contact.setPhone1(phnList[0]);
					contact.setPhone2(phnList[1]);
				}
				else
				{
					String [] phnList= phoneNumber.split("#");
					contact.setPhone1(phnList[0]);
					contact.setPhone2(null);
				}

				contact.setContactName(findData("איש קשר", dataArray));
				contact.setSalesArea(findData("אזור מכירה", dataArray));
				contact.setSettlement(findData("ישוב", dataArray));
			}
			else 
			{
				Println("Phone Couldn't retrived, adding to list");
			}
		}
		else
		{
			Println("Phone Couldn't retrived, adding to list");
		}
		return contact;
	}

	// string with symbol to int conversion 
	public static int stringToInt(String line, String sym)
	{
		try
		{
			Println("Starting  symbol to int");
			String []listLine =line.split(sym);
			String tmp="";
			for(int i=0;i<listLine.length;i++)
			{
				tmp +=listLine[i].trim();
			}
			Println("Converted Data  "+Integer.parseInt(tmp));
			return Integer.parseInt(tmp);
		}
		catch(Exception ex)
		{
			Println(ex.getMessage());
			return 0;
		}
	}

	//date
	public static String retrieveDate(List<WebElement> listDate )
	{
		Println("Starting Date");
		String tmpDate="";
		Iterator<WebElement> i = listDate.iterator();

		while(i.hasNext()) {
			WebElement row = i.next();
			tmpDate += row.getText().trim();
		}

		return tmpDate;
	}

	public static int getPrice(WebDriver driver, String path)
	{
		try
		{
			System.out.println("starting getPrice ");

			List<WebElement> drop = driver.findElements(By.xpath(path));
			Iterator<WebElement> i = drop.iterator();

			String prce="";
			while(i.hasNext()) {
				WebElement row = i.next();
				//System.out.println("printing web element");
				prce += row.getText();
			}

			String [] prcList=prce.split("₪");
			String price=prcList[0];

			if(price.length()<2)
				price=prcList[1];

			Println("getPrice : "+price+"  main string "+prce);

			return stringToInt(price, ",");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			return 0;
		}
	}

	public static void runExe(Thread t)
	{
		Println("Running the batch");
		try
		{
			if(PathInfo.batchRun)
			{
				Runtime runTIme = Runtime.getRuntime();
				Process process = runTIme.exec("calc.exe"); //absolute or relative path
				Thread.sleep(PathInfo.batchWait);

			}

		}
		catch(Exception ex)
		{
			Println("Couldn't run the exe");
			System.out.println(ex.getMessage());
		}
	}

	//the api, to decode the the captcha
	public static String  readCaptcha(String imageLink)
	{
		String key =PathInfo.apiKey;//"4833e5537ae972a643efad33a707d40f";//"5ed62b7616c50331a72b614b97754b7e";// 
		String img_fn = imageLink;

		System.out.println("Decoding");
		ApiResult ret = BypassCaptchaApi.Submit(key, img_fn);
		if(!ret.IsCallOk)
		{
			System.out.println("Error: " + ret.Error);
			return null;
		}

		String value = ret.DecodedValue;
		System.out.println("Using the decoded value: " + value);
		System.out.println("Suppose it is correct.");
		ret = BypassCaptchaApi.SendFeedBack(key, ret, true);
		if(!ret.IsCallOk)
		{
			System.out.println("Error: " + ret.Error);
			return null;
		}

		ret = BypassCaptchaApi.GetLeft(key);
		if(!ret.IsCallOk)
		{
			System.out.println("Error: " + ret.Error);
			return null;
		}

		System.out.println("There are " + ret.LeftCredits + " credits left on this key");
		System.out.println("OK");
		return value;
	}

	//checking is next button available
	public static boolean isNextButton(WebDriver driver)
	{
		boolean flag=false;

		String isNextPath=PathInfo.nextButton;

		List<WebElement> rows = driver.findElements(By.xpath(isNextPath));

		if(rows.size()>0)
			flag=true;
		else
			flag=false;

		return flag;
	}

	public static WebDriver getDriver() {
		WebDriver driver=null;
		switch (PathInfo.browserName) {
		case "chrome":
			driver= new ChromeDriver();
			break;
			
		case "firefox":
			driver=new FirefoxDriver();
			break;
		case "phantomjs":
			DesiredCapabilities caps=new DesiredCapabilities();
			caps.setJavascriptEnabled(true);
			caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,System.getProperty("user.dir")+"/driver/phantomjs.exe" );
			driver= new PhantomJSDriver(caps);
			break;
		case "internetExplorer":
			
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			driver=new InternetExplorerDriver(ieCapabilities);
			
			break;
			
		default:
			driver= new FirefoxDriver();
			break;
		}
		
		return driver;
	}
}
