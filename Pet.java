package webScrapping;

public class Pet {
	
	private String animalName;
	private String type;
	private String action;
	private float age;
	private String sex;
	private String dogBreeds;
	private long price;
	
	private String petMainDetails;
	private String petDetails;
	private Contact petContact;
	
int adNumber;
	
	public int getAdNumber() {
		return adNumber;
	}
	public void setAdNumber(int adNumber) {
		this.adNumber = adNumber;
	}
	
private String pubDate;
	
	public String getPubDate() {
		if(pubDate!=null)
			return Utils.fixToSql(this.pubDate);
		else return null;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	
	public String getPetMainDetails() {
		if(petMainDetails!=null)
			return Utils.fixToSql(this.petMainDetails);
		else return null;
	}
	public void setPetMainDetails(String petMainDetails) {
		this.petMainDetails = petMainDetails;
		
	}
	public String getPetDetails() {
		if(petDetails!=null)
			return Utils.fixToSql(this.petDetails);
			else return null;
	}
	public void setPetDetails(String petDetails) {
		this.petDetails = petDetails;
	}
	public Contact getPetContact() {
		return petContact;
	}
	public void setPetContact(Contact petContact) {
		this.petContact = petContact;
	}
	public String getAnimalName() {		
		if(animalName!=null)
			return Utils.fixToSql(this.animalName);
			else return null;
	}
	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}
	public String getType() {
		if(type!=null)
			return Utils.fixToSql(this.type);
			else return null;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAction() {
		if(action!=null)
			return Utils.fixToSql(this.action);
			else return null;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public float getAge() {
		return age;
	}
	public void setAge(float age) {
		this.age = age;
	}
	public String getSex() {
		if(sex!=null)
			return Utils.fixToSql(this.sex);
			else return null;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getDogBreeds() {
		if(dogBreeds!=null)
			return Utils.fixToSql(this.dogBreeds);
			else return null;
	}
	public void setDogBreeds(String dogBreeds) {
		this.dogBreeds = dogBreeds;
	}
	public long getPrice() {
		if(price !=0)
		{
			return price;
		}
		return 0;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	
}
