package webScrapping;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class CarThread extends Thread {
	private Thread car;
	private String threadName; 

	CarThread(String name)
	{
		this.threadName=name;
	}


	//to Avoid System.out.. 

	private static void Println(Object object)
	{
		System.out.println(object);
	}


	public void run()
	{
		WebDriver driver=null;

		String parrenturl=PathInfo.linkCar;
		int pageCount=2;
		String newUrl="http://www.yad2.co.il/Cars/Car.php?Page=";

		while(true)
		{
			Println("################################## While loop car Main #########################################");
			try
			{
				if(driver != null)
					driver.close();

				//driver= new FirefoxDriver();
				driver=Information.getDriver();
				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL
				driver.get(parrenturl);

				System.out.println("Running  Car for page "+(pageCount-1));

				// Maximize Window
				driver.manage().window().maximize();

				String pageUrl=driver.getCurrentUrl();
				if(pageUrl.equals(PathInfo.geolink))
				{
					Information.runExe(car);
					continue;
				}

				//String parentHandle = driver.getWindowHandle();

				//ArrayList<String> carList= new ArrayList<String>();

				String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";

				List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
				System.out.println("Total number of rows :"+ rows.size());

				if(rows.size()>0 && driver!=null){

					MultipleCar car1= new MultipleCar(parrenturl, 2, 24);

					MultipleCar car2= new MultipleCar(parrenturl, 25, 47);

					car1.start();
					car2.start();

					PathInfo.isCar1=false;
					PathInfo.isCar2=false;
					Println(parrenturl);
					
					
					boolean isNextButtonAvailable=Information.isNextButton(driver);

					if(isNextButtonAvailable)
					{
						parrenturl=newUrl+pageCount;
						pageCount++;
					}
					else
					{
						parrenturl=PathInfo.linkCar;
						pageCount=2;
						Println("Starting againg from home page");
					}
					
					driver.close();

					while(true)
					{
						//Println("At main thread while loop isCar1 complete "+PathInfo.isCar1+"  ,iscar2 complete "+PathInfo.isCar2);
						if(PathInfo.isCar1 && PathInfo.isCar2)
						{
							Println("At main thread while loop breaked");
							break;	
						}
						Thread.sleep(PathInfo.mainThreadSleep);
					}

				}

			}
			catch(Exception ex)
			{
				driver=null;
				System.out.println(ex.getMessage());
				Println("Conection Error");
				Information.runExe(car);
				//showConnectionError();
			}

		}
	}

	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (car == null)
		{
			car = new Thread (this, threadName);
			car.start ();
		}
	}
}
