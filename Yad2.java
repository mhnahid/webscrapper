package webScrapping;

public class Yad2 {

	private Contact yadContact;
	private String category;
	private String productItem;
	private String manufacturer;
	private String productStatus;
	private long price;
	private String description;
	private String details;
	private String model;
	
int adNumber;
	
	public int getAdNumber() {
		return adNumber;
	}
	public void setAdNumber(int adNumber) {
		this.adNumber = adNumber;
	}
	

	public String getModel() {
		if(model!=null)
			return Utils.fixToSql(this.model);
			else return null;
	}
	public void setModel(String model) {
		this.model = model;
	}
	private String pubDate;

	public String getPubDate() {
		if(pubDate!=null)
			return Utils.fixToSql(this.pubDate);
			else return null;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public String getDetails() {
		if(details!=null)
			return Utils.fixToSql(this.details);
			else return null;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getCategory() {
		if(category!=null)
			return Utils.fixToSql(this.category);
			else return null;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Contact getYadContact() {
		return yadContact;
	}
	public void setYadContact(Contact yadContact) {
		this.yadContact = yadContact;
	}
	public String getProductItem() {
		if(productItem!=null)
			return Utils.fixToSql(this.productItem);
			else return null;
	}
	public void setProductItem(String productItem) {
		this.productItem = productItem;
	}
	public String getManufacturer() {
		if(manufacturer!=null)
			return Utils.fixToSql(this.manufacturer);
			else return null;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getProductStatus() {
		if(productStatus!=null)
			return Utils.fixToSql(this.productStatus);
			else return null;
	}
	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public String getDescription() {
		if(description!=null)
			return Utils.fixToSql(this.description);
			else return null;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
