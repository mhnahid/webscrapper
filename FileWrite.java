package webScrapping;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
//import java.io.Writer;
//import java.text.SimpleDateFormat;
//import java.util.Date;

public class FileWrite {
	
	
	
	public static void CarCsvWrite(Car car)
	{
//		Date date= new Date();
//		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-mm-dd_'at'_HH");
		
		String fileName="CarData.csv"; 
		System.out.println(fileName);
		try
		{
			System.out.println(" Writing File");
			File file = new File(fileName);
			
			String fileData=car.getPubDate()+","+car.getCarContact().getSalesArea()+","+car.getCarContact().getSettlement()+","+car.getCarContact().getPhone1()+","+car.getCarContact().getPhone2()+","+car.getCarContact().getPhone2()+","+car.getCarContact().getContactName()+
					car.getManufacturer()+","+car.getModel()+","+car.getSubModel()+","+car.getVolume()+","+car.getYear()+","+car.getH_AliihRoad()+","+car.getHand()+","+car.getPrice()+","+car.getMileage()+","+car.getDescription()+"\n";
			// if file doesnt exists, then create 
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream fw = new FileOutputStream(file.getAbsoluteFile(),true);
			OutputStreamWriter bw = new OutputStreamWriter(fw, "UTF-8");
			bw.append(fileData);
			//bw.write(outPut);
			bw.close();
			
			/*Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("outfilename"), "UTF-8"));
			try {
				out.write(aString);
			} finally {
				out.close();
			}*/

		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	public static void petCsvWrite(Pet pet)
	{
//		Date date= new Date();
//		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-mm-dd_'at'_HH");
		
		String fileName="PetData.csv"; 
		try
		{
			File file = new File(fileName);
			
			String fileData=pet.getPubDate()+","+pet.getPetContact().getSalesArea()+","+pet.getPetContact().getSettlement()+","+pet.getPetContact().getPhone1()+","+pet.getPetContact().getPhone2()+","+pet.getPetContact().getPhone2()+","+pet.getPetContact().getContactName()+
					pet.getPetMainDetails()+pet.getPrice()+"\n"; //pet.getAnimalName()+","+pet.getType()+","+pet.getAction()+","+pet.getAge()+","+pet.getSex()+","+pet.getDogBreeds()
				
			// if file doesnt exists, then create 
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.append(fileData);
			//bw.write(outPut);
			bw.close();
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	public static void YadCsvWrite(Yad2 yad)
	{
//		Date date= new Date();
//		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-mm-dd_'at'_HH");
		
		String fileName="Yad2Data.csv"; 
		try
		{
			File file = new File(fileName);
			
			String fileData=yad.getPubDate()+","+yad.getYadContact().getSalesArea()+","+yad.getYadContact().getSettlement()+","+yad.getYadContact().getPhone1()+","+yad.getYadContact().getPhone2()+","+yad.getYadContact().getPhone2()+","+yad.getYadContact().getContactName()+
					yad.getDetails()+","+yad.getDescription()+"\n";//yad.getCategory()+","+yad.getProductItem()+","+yad.getManufacturer()+","+yad.getProductStatus()+","+yad.getPrice()
			// if file doesnt exists, then create 
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.append(fileData);
			//bw.write(outPut);
			bw.close();
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	public static void NadlanCsvWrite(Nadlan nadlan)
	{
		//Date date= new Date();
		//SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-mm-dd_'at'_HH");
		
		String fileName="NadlanData.csv"; 
		try
		{
			File file = new File(fileName);
			String fileData=nadlan.getPubDate()+","+nadlan.getNadlanContact().getPhone1()+" , "+nadlan.getNadlanContact().getPhone2()+" ,"+nadlan.getNadlanContact().getContactName()+","+nadlan.getDetails()+","+nadlan.getPrice()+", "+nadlan.getMoreDetails()+"\n";
			
			/*String fileData=nadlan.getNadlanContact().getSalesArea()+","+nadlan.getNadlanContact().getSettlement()+","+nadlan.getNadlanContact().getPhone1()+","+nadlan.getNadlanContact().getPhone2()+","+nadlan.getNadlanContact().getPhone2()+","+nadlan.getNadlanContact().getContactName()+
					nadlan.getPropertyType()+","+nadlan.getNeighborhood()+","+nadlan.getAddress()+","+nadlan.getRooms()+","+nadlan.getFloor()+","+nadlan.getSize()+","+nadlan.getPrice()+", parking: "+nadlan.isParking()+", Bar : "+nadlan.isAir()+", Dimension : "+nadlan.isAccessForDisabled()+
					", Elevator : "+nadlan.isElevator()+", Warehouse : "+nadlan.isWarehouse()+" , Sun Terrace : "+nadlan.isSunTerrace()+", Porch : "+nadlan.isPorch()+", Renovated : "+nadlan.isRenovated()+", Pandor Door : "+nadlan.isPandorDoors()+", Uint : "+nadlan.isUint()+",Furniture : "+nadlan.getFurniture()+", Availability : "+nadlan.getAvailability()
					+", Extra : "+nadlan.getExtras()+", Garden : "+nadlan.getGarden()+"\n";*/
			// if file doesn't exists, then create 
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.append(fileData);
			//bw.write(outPut);
			bw.close();
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

}
