package webScrapping;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TestNadlan extends Thread{

	private Thread nadlanT;
	private String threadName;

	TestNadlan(String name)
	{
		this.threadName=name;
	}

	//to Avoid System.out.. 
	private static void Println(Object object)
	{
		System.out.println(object);
	}


	public void run()
	{
		WebDriver driver =null;

		String parrenturl=PathInfo.linkNadlan;
		int pageCount=2;
		String newUrl="http://www.yad2.co.il/Yad2/Yad2.php?Page=";

		while(true)
		{

			Println("################################## While loop  main thread nadlan #########################################");
			try
			{
				if(driver!=null)
					driver.close();

				// Initialize WebDriver
//				driver = new FirefoxDriver();
				driver=Information.getDriver();

				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL

				driver.get(parrenturl);

				System.out.println("Running  Nad");

				// Maximize Window
				driver.manage().window().maximize();

				String pageUrl=driver.getCurrentUrl();
				if(pageUrl.equals(PathInfo.geolink))
				{
					Information.runExe(nadlanT);
					continue;
				}

				String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";

				List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
				System.out.println("Total number of rows :"+ rows.size());

				// clicking on each row
				if(rows.size()>0 && driver!=null){

					MultipleNadlan nad1= new MultipleNadlan(parrenturl,2, rows.size()/2);

					MultipleNadlan nad2= new MultipleNadlan(parrenturl, rows.size()/2, rows.size());

					nad1.start();
					nad2.start();

					PathInfo.isNadlan1=false;
					PathInfo.isNadlan2=false;
					
					
					boolean isNextButtonAvailable=Information.isNextButton(driver);

					if(isNextButtonAvailable)
					{
						parrenturl=newUrl+pageCount;
						pageCount++;
					}
					else
					{
						parrenturl=PathInfo.linkNadlan;
						pageCount=2;
						Println("Starting againg from home page");
					}

					driver.close();

					while(true)
					{
						if(PathInfo.isNadlan1 && PathInfo.isNadlan2)
							break;
						Thread.sleep(PathInfo.mainThreadSleep);
					}

				}


			}

			catch (Exception ex) {
				driver=null;
				System.out.println(ex.getMessage());
				Println("Conection Error");
				Information.runExe(nadlanT);
			}	
		}
	}

	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (nadlanT == null)
		{
			nadlanT = new Thread (this, threadName);
			nadlanT.start ();
		}
	}



}
