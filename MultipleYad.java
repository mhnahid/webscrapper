package webScrapping;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MultipleYad  extends Thread {
	private Thread multipleYad;
	private String parentLink;
	private int startRow;
	private int endRow;
	private String threadName;
	
	MultipleYad(String parentLink, int startRow, int endRow)
	{
		this.parentLink=parentLink;
		this.startRow=startRow;
		this.endRow=endRow;
		
		threadName="MultipleYad with range from "+startRow+" to  "+endRow; 
	}
	
	public void start ()
	{
		System.out.println("Starting " +  threadName );
		if (multipleYad == null)
		{
			multipleYad = new Thread (this, threadName);
			multipleYad.start ();
		}
	}
	
	private static void Println(Object object)
	{
		System.out.println(object);
	}
	
	@SuppressWarnings("null")
	public void run()
	{
		String parrenturl=parentLink;
		int pageCount=2;
		int exceptionPage=0;
		boolean willDestroy=false;
		int lastClickedRow=startRow;

		WebDriver driver =null;

		while(true)
		{

			if(willDestroy)
				break;
			try
			{
				if(driver != null)
					driver.close();

				// Initialize WebDriver
				// driver = new FirefoxDriver();
				//driver= new ChromeDriver();
				driver=Information.getDriver();

				// Wait For Page To Load
				driver.manage().timeouts().implicitlyWait(PathInfo.waitTime, TimeUnit.SECONDS);

				//driver.manage().
				driver.manage().deleteAllCookies();

				// Go to URL
				driver.get(parrenturl);

				System.out.println("Running  Yad at multiple thread");

				// Maximize Window
				driver.manage().window().maximize();

				String parentHandle = driver.getWindowHandle();
				String pageUrl=driver.getCurrentUrl();
				
				if(pageUrl.equals(PathInfo.geolink))
				{
					Information.runExe(multipleYad);
					continue;
				}

				String rowCountPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr";

				List<WebElement> rows = driver.findElements(By.xpath(rowCountPath));
				System.out.println("Total number of rows :"+ rows.size());

				// clicking on each row
				if(rows.size()>0 && driver !=null)
				{
					int rowCount=startRow;
					
					if(pageCount==exceptionPage)
					{
						rowCount=lastClickedRow-1;
					}
					Println("Row Count "+rowCount);
					
					for(;rowCount<=rows.size();rowCount++)
					{
						Instant start = Instant.now();
						
						String publishedDate="";
						try
						{
							Yad2 yad= new Yad2();

							String dynamicIdPath="//*[@id=\"all_results_table\"]/div[4]/table/tbody/tr["+rowCount+"]";

							String frqpath=dynamicIdPath+PathInfo._freqTdPrefix1+16+PathInfo._freqSuffix1+PathInfo._freqAn;
							String frqDate=dynamicIdPath+"/td[15]";


							System.out.println("Clicking on row  "+rowCount);

							List<WebElement> linkValue = driver.findElements(By.xpath(frqpath));

							// Checking if there is a link to click, cause there are many black rows
							if(linkValue.size()>0)
							{
								lastClickedRow=rowCount;
								List<WebElement> listDate = driver.findElements(By.xpath(frqDate));
								publishedDate=Information.retrieveDate(listDate);

								driver.findElement(By.xpath(frqpath)).click();

								boolean addFlag=false;

								// to check if there is an add page then close that page
								Println("Checking for add");

								for (String winHandle : driver.getWindowHandles())
								{

									driver.switchTo().window(winHandle);
									String newWindow=driver.getCurrentUrl();
									Println(winHandle+" it's url : "+newWindow);

									if(newWindow.equals(parrenturl))
									{
										Println("Continue");
										continue;
									}
									else
									{
										String [] linkArray=newWindow.split("/");

										for(int i=0;i<linkArray.length;i++)
										{
											if(linkArray[i].equals("Yad2"))
											{
												addFlag=true;
												Println("valid page and Continue");
												break;
											}
										}
									}


									if(!addFlag)
									{
										Println("Wrong page !!!!!\n Closed!!!");
										driver.close();
										driver.switchTo().window(parentHandle);
										continue;
									}
								}

								Println(" Closing Checking for add"); // ending of closing page

								//Starting for getting data


								// Getting all new window
								boolean flag=false;
								int count=1;

								for (String winHandle : driver.getWindowHandles())
								{

									driver.switchTo().window(winHandle);
									String newWindow=driver.getCurrentUrl();

									Println("Page Count "+count+" New Window url : "+newWindow);

									if(newWindow.equals(parrenturl))
									{
										Println("Continue");
										count++;
										continue;
									}
									else
									{
										String [] linkArray=newWindow.split("/");

										for(int i=0;i<linkArray.length;i++)
										{
											if(linkArray[i].equals("Yad2"))
											{
												flag=true;
												break;
											}
										}
									}

									if(flag==true)
									{
										Println("Found and Getting data");
										break;
									}
									else
									{
										Println("Wrong page !!!!! continue");
										count++;
										continue;
									}
								}
								// all code here
								// Yad Contact

								Contact yContact= new Contact();
								yContact=Information.setContact(driver, Information.dataArray(driver, PathInfo.yadContactPath), PathInfo.yadPhonePath);

								if(yContact.getPhone1() !=null)
								{
									try
									{
										Println("Starting getting all data");
										String [] detailsArray=Information.dataArray(driver, PathInfo.yadDetails);
										yad.setPubDate(publishedDate);

										yad.setYadContact(yContact);

										yad.setCategory(Information.findData("קטגוריה", detailsArray));
										yad.setProductItem(Information.findData("מוצר/פריט", detailsArray));

										yad.setManufacturer(Information.findData("יצרן", detailsArray));
										yad.setProductStatus(Information.findData("מצב המוצר", detailsArray));
										yad.setModel(Information.findData("דגם",  detailsArray));

										yad.setPrice(Information.getPrice(driver, PathInfo.yadPricePath));
										
										//yad.setDescription(Information.newLineRemover(driver, PathInfo.yadDescriptionPath));
										
										yad.setAdNumber(Information.getAdNumber(driver));


										System.out.println("Object added");
										//FileWrite.YadCsvWrite(yad);
										pushToDB(yad);
									}
									catch(Exception ex)
									{
										Println(ex.getMessage());
										Println("Phone Couldn't retrived, adding to list");
									}

								}
								else {
									Println("Phone Couldn't retrived, adding to list");
									
									if(!PathInfo.batchRun)
									{
										PathInfo.batchRun=true;
										Information.runExe(multipleYad);
										PathInfo.batchRun=false;
									}
									else
									{
										Thread.sleep(PathInfo.batchWait);
									}

								}

								driver.close(); // close newly opened window when done with it
								driver.switchTo().window(parentHandle); // switch back to the original window
								
								Instant end = Instant.now();
								Duration timeElapsed = Duration.between(start, end);
								Println(">>>>>>>>>>>> Time taken: "+ timeElapsed.toMillis()/1000 +" seconds");
							}

						}catch(Exception ex)
						{
//							String currentLink=driver.getCurrentUrl()+" ^"+publishedDate;
//							yadList.add(currentLink);

							System.out.println(ex.getMessage());
							System.out.println(" #########################################\nException at Yad\n#########################"+ex.getMessage());
							
							if(driver !=null)
							{
								driver.manage().deleteAllCookies();
								driver.close();
								driver.switchTo().window(parentHandle);
								if(driver != null)
								{
									driver.close();
								}
							}
							else
							{
								driver.switchTo().window(parentHandle);
								driver.manage().deleteAllCookies();
								driver.close();
							}
							
							
							//driver= new FirefoxDriver();
							//driver=new ChromeDriver();
							driver=Information.getDriver();
							driver.get(parrenturl);// switch back to the original window
							//showMessage();
						}

						if(rowCount==endRow)
						{
							if(startRow>(rows.size()/2)-1)
							{
								PathInfo.isYad2=true;
							}
							else
							{
								PathInfo.isYad1=true;
							}
							if(driver !=null)
							{
								driver.manage().deleteAllCookies();
								driver.close();
								if(driver != null)
								{
									driver.switchTo().window(parentHandle);
									driver.close();
								}
							}
							willDestroy=true;
							break;
						}
					}
				}
				else
				{
					Println("Conection Error");
				}
			
			
			}
			catch(Exception ex){
				exceptionPage=pageCount;
				driver=null;
				System.out.println(ex.getMessage());
				Println("Conection Error");
				Information.runExe(multipleYad);
			}
		}
	}

	
	private void pushToDB(Yad2 yad) throws IOException 
	{
		Connection conn = null;
		try 
		{
			System.out.println("in pushDb");
			String current;

			current = new java.io.File( "." ).getCanonicalPath();
			System.out.println("Current dir:"+current);
			current.replace('\\', '/');

			conn = DriverManager.getConnection("jdbc:ucanaccess://"+current+"/Database3.mdb");
			Statement s = conn.createStatement();

			String query="INSERT INTO tbl_secHand("
					+ "adNumber"
					+ ",phone1"
					+ ",phone2"
					+ ",contactName"
					+ ",pubDate"
					+ ",salesArea"
					+ ",settlement"
					+ ",category"
					+ ",productItem"			// NUmber
					+ ",manufacturer"
					+ ",productStatus"
					+ ",price"
					+ ",description)"
					+ "Values("+
					yad.getAdNumber()+",'"
					+yad.getYadContact().getPhone1()+"','"
					+yad.getYadContact().getPhone2()+"','"
					+yad.getYadContact().getContactName()+"','"
					+yad.getPubDate()+"','"
					+yad.getYadContact().getSalesArea()+"','"
					+yad.getYadContact().getSettlement()+"','"
					+yad.getCategory()+"','"
					+yad.getProductItem()+"','"
					+yad.getManufacturer()+"','"+
					yad.getProductStatus()+"',"+
					yad.getPrice()+",'"
					+yad.getDescription()+"')";
			System.out.println("execute query:"+query);
			s.executeUpdate(query);
			s.getConnection().commit();
			conn.close();
		} 
		catch (SQLException e) 
		{
			System.out.println(e.getMessage());

			//DATE 24-03-2016
			//Sharif - Big BUG was not to close DB connection ... don't leave any connection opened if crashes or throw exception
			if(conn != null)
				try {
					conn.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}

	}
	

}
